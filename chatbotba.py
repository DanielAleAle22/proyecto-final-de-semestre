from nltk.chat.util import Chat, reflections
mis_reflexions = {
"ir": "fui",
"hola": "hey"
}

pares = [
    [
        r"Soy un cliente con diabetes",
        ["Si tienes diabetes lo mas seguro es que uses los siguientes medicamentos: insulina,metformina,tolbutamida,glimepirida,glipizida",]
    ],
     [
        r"Cuando debo ponerme la insulina",
        ["La insulina debes inyectartela diario en ayunas 10mg,¡No lo olvides!",]
    ],
    [
        r"Cuando y que dias me toca la metformina",
        ["La metformina se toma diario cada 8 horas",]
    ],
    [
        r"Cuando y que dias me toca la tolbutamida",
        ["La tolbutamida se toma los martes y jueves y te la debes tomar a las 8:00am",]
    ],
    [
        r"Cuando y que dias me toca la glimepirida",
        ["La glimepirida se toma los dias lunes y viernes y te la debes tomar a la 1:00pm",]
    ],
    [
        r"Cuando y que dias me toca la glipizida",
        ["La glipizida se toma los jueves y te la debes tomar a las 10:00am",]
    ],
    [
        r"(.*)Gracias(.*)",
        ["Ten un excelente dia y estoy para atenderte porfavor cuida tu salud",]
    ],
    [
        r"Soy un cliente con dolor de cabeza(.*)",
        ["Si tienes dolor de cabeza deberias de tomar acido aceltalisilico",]
    ],
    [
        r"Cuanta dosis deberia de tomar de acido aceltalisilico(.*)",
        ["Se toma en el momento que te duela la cabeza y cada 8hrs",]
    ],
    [
        r"Cuando y que dias me toca la buprenorfina(.*)",
        ["La buprenorfina se toma los jueves y te la debes tomar cada jueves solamente",]
    ],
[
        r"Cuando y que dias me toca la capsaicina(.*)",
        ["La capsaicina se toma los martes y te la debes tomar cada martes solamente",]
    ],
[
        r"Cuando y que dias me toca la CLONIXINATO DE LISINA.(.*)",
        ["El CLONIXINATO DE LISINA se toma los dias lunes,martes y miercoles y te la debes tomar cada 12hrs",]
    ],
[
        r"Cuando y que dias me toca la DEXMEDETOMIDINA(.*)",
        ["La DEXMEDETOMIDINA se toma los jueves a las 9:00pm",]
    ],
[
        r"Cuando y que dias me toca la dextropropoxifeno(.*)",
        ["La DEXTROPROPOXIFENO se toma los martes y te la debes tomar a las 6:00pm",]
    ],
[
        r"Cuando y que dias me toca el ETOFENAMATO.(.*)",
        ["La ETOFENAMATO se toma los martes y miercoles y te la debes tomar a las 2:00pm",]
    ],
[
        r"Cuando y que dias me toca la FENTANILO(.*)",
        ["La FENTANILO se toma los lunes y te la debes tomar a las 10:00pm",]
    ],
[
        r"Cuando y que dias me toca el IBUPROFENO(.*)",
        ["El IBUPROFENO se toma los miercoles y jueves y te la debes tomar a las 10:00pm",]
    ],
[
        r"Cuando y que dias me toca el KETOROLACO(.*)",
        ["La KETOROLACO se toma los viernes,sabado y domingo y te la debes tomar a las 11:00am",]
    ],
[
        r"Cuando y que dias me toca el METAMIZOL SÓDICO(.*)",
        ["La METAMIZOL SÓDICO se toma los lunes y te la debes tomar a las 5:00pm",]
    ],
[
        r"Cuando y que dias me toca la MORFINA(.*)",
        ["La MORFINA se inyecta los lunes y jueves y te la debes inyectar a las 2:00pm",]
    ],
[
        r"Cuando y que dias me toca la NALBUFINA(.*)",
        ["La NALBUFINA se toma los jueves y sabados y te la debes tomar a las 8:00pm",]
    ],
[
        r"Cuando y que dias me toca la OXICODONA(.*)",
        ["La OXICODONA se toma los viernes y sabados y te la debes tomar a las 3:00pm",]
    ],
[
        r"Cuando y que dias me toca el PARACETAMOL(.*)",
        ["El PARACETAMOL se toma todos los dias y te la debes tomar a la 1:00pm",]
    ],
[
        r"Cuando y que dias me toca la TRAMADOL(.*)",
        ["La TRAMADOL se toma los jueves,viernes y sabados y te la debes tomar a las 8:00pm",]
    ],
[
        r"Cuando y que dias me toca el TRAMADOL-PARACETAMOL(.*)",
        ["El TRAMADOL-PARACETAMOL se toma los jueves y sabados y te la debes tomar a las 4:00pm",]
    ],
[
        r"Para que sirve el ÁCIDO ACETILSALICÍLICO(.*)",
        ["El ÁCIDO ACETILSALICÍLICO ayuda a lograr que haya mayor flujo de sangre a las piernas. Puede usarse para tratar un ataque cardíaco y prevenir coágulos de sangre cuando usted tenga un ritmo cardíaco anormal. ",]
    ],
[
        r"Para que sirve la BUPRENORFINA(.*)",
        ["Los parches de BUPRENORFINA se usan para aliviar el dolor fuerte en personas que se espera que necesiten medicamento para el dolor todo el tiempo durante un período prolongado y que no puedan recibir tratamiento con otros medicamentos. Pertenece a una clase de medicamentos llamados analgésicos opiáceos (narcóticos).",]
    ],
[
        r"Para que sirve el CAPSAICINA(.*)",
        ["Los parches de CAPSAICINA pueden proporcionar un alivio moderado del dolor, o mejor, a una minoría de pacientes con neuralgia posherpética, y evidencia de muy baja calidad de que benefician a los pacientes con neurosis por VIH y neuropatía diabética periférica.",]
    ],
[
        r"Para que sirve el CLONIXINATO DE LISINA(.*)",
        ["El CLONIXINATO DE LISINA está indicado como analgésico y antiinflamatorio en pacientes que cursan con dolor agudo o crónico. Afecciones de tejidos blandos, cefalea, otalgias, sinusitis y herpes zoster. Dolor e intervenciones ginecológicas, ortopé­dicas, urológicas y de cirugía general. ",]
    ],
[
        r"Para que sirve la DEXMEDETOMIDINA(.*)",
        ["La DEXMEDETOMIDINA es una fármaco economizador de opiáceos (reduce la necesidad de opiáceos).",]
    ],
[
        r"Para que sirve el DEXTROPROPOXIFENO(.*)",
        ["El DEXTROPROPOXIFENO Esta revisión evaluó la eficacia analgésica y los efectos adversos de una dosis única oral de dextropropoxifeno sola o en combinación con paracetamol para el tratamiento del dolor posoperatorio moderado a intenso. ",]
    ],
[
        r"Para que sirve el ETOFENAMATO(.*)",
        ["El ETOFENAMATO es un antiinflamatorio no esteroideo con propiedades analgésicas que se utiliza para el tratamiento tópico de procesos dolorosos, tanto reumáticos como traumáticos y penetra fácilmente a través de la piel. ",]
    ],
[
        r"Para que sirve el FENTANILO(.*)",
        ["El FENTANILO  es un fuerte analgésico opioide sintético similar a la morfina, pero entre 50 y 100 veces más potente. En su forma recetada se utiliza para calmar el dolor, pero el fentanilo también se produce ilegalmente y se distribuye como droga callejera.",]
    ],
[
        r"Para que sirve el IBUPROFENO(.*)",
        ["El IBUPROFENO de venta libre se usa para bajar la fiebre y para aliviar dolores leves como dolores de cabeza, dolores musculares, artritis, cólicos menstruales, molestias del resfriado común, dolores de dientes y dolores de espalda.",]
    ],
[
        r"Para que sirve el KETOROLACO",
        ["El KETOROLACO se usa para aliviar el dolor moderadamente fuerte, por lo general después de una operación quirúrgica. Pertenece a una clase de medicamentos llamados antiinflamatorios sin esteroides. Funciona al detener la producción de una sustancia que causa dolor, fiebre e inflamación.",]
    ],
[
        r"Para que sirve el METAMIZOL SÓDICO",
        ["El METAMIZOL SÓDICO se utiliza para el tratamiento del dolor agudo moderado o intenso post-operatorio o post-traumático, de tipo cólico o de origen tumoral. También se utiliza en los casos de fiebre alta que no responda a otras medidas u otros medicamentos para la fiebre.",]
    ],
[
        r"Para que sirve la MORFINA",
        ["La MORFINA se usa para aliviar el dolor de moderado a fuerte. La morfina pertenece a una clase de medicamentos llamados analgésicos opiáceos (narcóticos). Funciona al cambiar la manera en que el cerebro y el sistema nervioso responden al dolor.",]
    ],
[
        r"Para que sirve la NALBUFINA",
        ["La inyección de NALBUFINA se usa para aliviar el dolor de moderado a fuerte. También se usa con otros medicamentos y agentes anestésicos antes, durante y después de la cirugía y otros procedimientos médicos. La inyección de nalbufina pertenece a una clase de medicamentos llamados agonistas-antagonistas de los opiáceos. Funciona al cambiar la manera en que el cuerpo responde al dolor.",]
    ],
[
        r"Para que sirve el OXICODONA",
        ["El OXICODONA se usa para tratar el dolor que va de moderado a grave. Se compone de morfina y se une con los receptores de opioides del sistema nervioso central. El clorhidrato de oxicodona es un tipo de analgésico y un tipo de opiáceo.",]
    ],
[
        r"Para que sirve el PARACETAMOL",
        ["El PARACETAMOL Este tipo de medicamento se utiliza para reducir la fiebre y aliviar el dolor.",]
    ],
[
        r"Para que sirve el TRAMADOL",
        ["El TRAMADOL se utiliza para aliviar el dolor de moderado a moderadamente intenso. Las tabletas de acción prolongada y las cápsulas de tramadol solo las usan las personas que se espera que necesiten el medicamento para aliviar dolor todo el tiempo. El tramadol pertenece a una clase de medicamentos llamados analgésicos opiáceos (narcóticos). Funciona al cambiar la manera en que el cerebro y el sistema nervioso responden al dolor.",]
    ],
]
def chatear():
    print("Hola, soy tu protector de salud") #mensaje por defecto
    chat = Chat(pares, mis_reflexions)
    chat.converse()
if __name__ == "__main__":
    chatear()

chatear()